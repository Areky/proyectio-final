/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import modelo.conexiomysql;
/**
 *
 * @author MARIZZIO-1
 */
public class consultas {
    public String todosAlumnos(){
        String sql = "SELECT cuenta, nombre, nombre_carrera as carrera "
                + "FROM alumnos NATURAL JOIN carreras;";
        String cadena = "";
        try {
            PreparedStatement statement = conexiomysql.conectar().prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            ResultSetMetaData rsm = rs.getMetaData();
            int numalum = 1;
            while ( rs.next() ) {
                cadena += "\nAlumno " + numalum + ":\n";
                int numColumns = rs.getMetaData().getColumnCount();
                for ( int i = 1 ; i <= numColumns ; i++ ) {
                    cadena += rsm.getColumnLabel(i)+": "+rs.getString(i)+", ";
                }
                numalum++;
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        } finally {
            conexiomysql.desconectar();
        }
        return cadena;
    }
    
}

