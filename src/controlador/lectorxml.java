package controlador;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author mike
 */
public class lectorxml extends Thread{
    File xmlFile;
    DocumentBuilderFactory dbFactory;
    DocumentBuilder dBuilder;
    Document doc;
    
    public lectorxml(String ruta){
        try {
            //Se inicializan todos los elementos necesario para leer XML
            xmlFile = new File(ruta);
            dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(xmlFile);
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            System.out.println(ex);
        }
    }

    public lectorxml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void cargaClientes(){
        doc.getDocumentElement().normalize();
        //Lista de nodos con la etiqueta cliente
	NodeList nList = doc.getElementsByTagName("cliente");
        //Aqui cambio para cargar a la BD
	for (int temp = 0; temp < nList.getLength(); temp++) {
            //De toda la lista tomamos uno para ver su contenido
            Node nNode = nList.item(temp);
            //Si el nodo esta bien formado
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                //Obtenemos los diferentes elementos del nodo
                Element eElement = (Element) nNode;
                String cliente = eElement.getElementsByTagName("nombre").item(0).getTextContent();
                String direccion = eElement.getElementsByTagName("direccion").item(0).getTextContent();
                //Se imprime el contenido
                System.out.println(cliente +" " +direccion);
            }
	}  
    }

    public void cargaFacturas() {
        doc.getDocumentElement().normalize();
        //Lista de nodos con la etiqueta cliente
	NodeList nList = doc.getElementsByTagName("factura");
        //Aqui cambio para cargar a la BD
	for (int temp = 0; temp < nList.getLength(); temp++) {
            //De toda la lista tomamos uno para ver su contenido
            Node nNode = nList.item(temp);
            //Si el nodo esta bien formado
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                //Obtenemos los diferentes elementos del nodo
                Element eElement = (Element) nNode;
                String cliente = eElement.getElementsByTagName("nombre").item(0).getTextContent();
                String direccion = eElement.getElementsByTagName("direccion").item(0).getTextContent();
                //Se imprime el contenido
                System.out.println(cliente +" " +direccion);
            }
        }
    }
}
