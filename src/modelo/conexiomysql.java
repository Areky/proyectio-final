/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Clase que abre la conexion a alguna base de MySQL
 * @author mike
 */
public class conexiomysql {
    // Constantes para la conexion
    private static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DATABASE_URL = "jdbc:mysql://localhost:3306/FCiencias";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "todo";
    private static final String MAX_POOL = "250";

    // Objeto del paquete Cobecction que me permite establecer la conexion
    private static Connection conexion;
    // Objecto que sirve para almacenar las propiedades de la conexion
    private static Properties properties;

    /**
     * Este metodo devuelve las propiedades de la conexion
     * @return properties
     */
    private static Properties getProperties() {
        if (properties == null) {
            properties = new Properties();
            properties.setProperty("user", USERNAME);
            properties.setProperty("password", PASSWORD);
            properties.setProperty("MaxPooledStatements", MAX_POOL);
        }
        return properties;
    }
    
    /**
     * Inicializa y devuelve el objeto que permite realizar consultas a la
     * base de datos.
     * @return connection
     */
    public static Connection conectar() {
        if (conexion == null) {
            try {
                Class.forName(DATABASE_DRIVER);
                conexion = DriverManager.getConnection(DATABASE_URL, getProperties());
                System.out.println("Conexion exitosa");
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        }
        return conexion;
    }

    /**
     * Si la conexion ya se establecio, este metodo cierra la conexion y
     * libera la memoria.
     */
    public static void desconectar() {
        if (conexion != null) {
            try {
                conexion.close();
                conexion = null;
                System.out.println("ya me deconecte :)");
            } catch (SQLException e) {
                e.printStackTrace(); 
            }
        }
    }
}
